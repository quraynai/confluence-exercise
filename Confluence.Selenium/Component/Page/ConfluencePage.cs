﻿using System.Configuration;

namespace Confluence.Selenium.Component.Page
{
    public abstract class ConfluenceWikiPage : IPage
    {
        public string Url => ConfigurationManager.AppSettings["ConfluenceWikiPage"] + Path;

        public abstract string Path { get; }
    }
}
