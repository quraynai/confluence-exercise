﻿using OpenQA.Selenium;

namespace Confluence.Selenium.Component.Page
{
    public class BlankPage : ConfluenceWikiPage
    {
        public override string Path => "";

        public By Title = By.CssSelector("textarea[data-test-id='editor-title']");
        public By PublishButton = By.Id("publish-button");
    }
}
