﻿using Confluence.Selenium.Engine;

namespace Confluence.Selenium.Component.Page
{
    public interface IPage
    {
        string Url { get; }
    }
}
