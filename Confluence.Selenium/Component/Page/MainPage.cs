﻿using OpenQA.Selenium;

namespace Confluence.Selenium.Component.Page
{
    public class MainPage : ConfluenceWikiPage
    {
        public override string Path => "";

        public By CreateDialogButton => By.Id("createGlobalItem");
        public By CreateItemButton => By.XPath("//*[@id=\"create-dialog\"]/div/div[2]/button");
        public By CreateDialog => By.Id("create-dialog");
        public By BlankPageTemplate => By.CssSelector("li[data-item-module-complete-key='com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page']");
    }
}
