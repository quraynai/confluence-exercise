﻿namespace Confluence.Selenium.Component.Page
{
    public static class Pages
    {
        public static LoginDetailPage LoginPage => new LoginDetailPage();
        public static MainPage MainPage => new MainPage();
        public static BlankPage BlankPage => new BlankPage();
        public static FilePage FilePage => new FilePage();
    }
}
