﻿using OpenQA.Selenium;
using System;

namespace Confluence.Selenium.Component.Page
{
    public class FilePage : ConfluenceWikiPage
    {
        public override string Path => "";

        public By Title => By.Id("title-text");
        public By RestrictionButton => By.CssSelector("button[data-test-id=\"restrictions.dialog.button\"]");
        public By RestrictionOption => By.CssSelector("div[data-test-id=\"restrictions-dialog.content-mode-select\"]");
        public By ApplyButton => By.XPath("//button/span/span[contains(text(),'Apply')]");
    }
}
