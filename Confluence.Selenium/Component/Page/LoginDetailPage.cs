﻿using OpenQA.Selenium;

namespace Confluence.Selenium.Component.Page
{
    public class LoginDetailPage : IPage
    {
        public string Url => "https://id.atlassian.com/login";

        public By UsernameTextbox => By.Id("username");
        public By SubmitButton => By.Id("login-submit");
        public By PasswordTextbox => By.Id("password");
    }
}
