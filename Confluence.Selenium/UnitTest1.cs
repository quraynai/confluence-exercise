﻿using Confluence.Selenium.Component.Page;
using Confluence.Selenium.Engine;
using Confluence.Selenium.Model;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Confluence.Selenium
{
    [TestFixture]
    public class UnitTest1 : TestController
    {
        private Context _context;

        static object[] ExerciseSource =
        {
            new object[] { "quraynai@gmail.com", "test123*", "Try Selenium To Create New Page",
@"Quality Engineers at Atlassian are our (not so) secret weapon. They not only help teams continually
improve the quality of the software they create, but they also help teams do it more effectively. In short,
our Quality Engineers are essential for shipping quality software faster.
Our developers know this. QE is routinely voted the most satisfying aspect of how we develop software
at Atlassian. As such, developers constantly pull Quality engineers into collaborating on their work. This is
not a company where code is thrown over the wall to QA/QE, nor a place for people who don’t want to be
deeply involved in the development process.
To be effective, our Quality Engineers need strong testing, technical and collaboration skills. The aim of
this set of exercises is to allow you to demonstrate some of those. " },
        };

        [SetUp]
        public void SetUp()
        {
            _context = GetContext();
        }

        [TestCaseSource("ExerciseSource")]
        public void Exercise(string email, string password, string title, string body)
        {
            _context.Redirect(Pages.LoginPage)
                .Type(Pages.LoginPage.UsernameTextbox, email)
                .Click(Pages.LoginPage.SubmitButton)
                .Type(Pages.LoginPage.PasswordTextbox, password)
                .Click(Pages.LoginPage.SubmitButton)
                .Wait(6000)
                .Redirect(Pages.MainPage)
                .Click(Pages.MainPage.CreateDialogButton)
                .Wait(Pages.MainPage.CreateDialog, Until.Visible)
                .Click(Pages.MainPage.BlankPageTemplate)
                .Click(Pages.MainPage.CreateItemButton, untilClickable: true)
                .Wait(6000)
                .Type(Pages.BlankPage.Title, title, untilInteractable: true)
                .Type(Keys.Tab)
                .Type(body)
                .Click(Pages.BlankPage.PublishButton)
                .Wait(Pages.FilePage.Title, Until.Visible)
                .ExpectText(Pages.FilePage.Title, title, ignoreCase: true)
                .Click(Pages.FilePage.RestrictionButton)
                .Click(Pages.FilePage.RestrictionOption)
                .Type(Keys.Down)
                .Type(Keys.Down)
                .Type(Keys.Enter)
                .Click(Pages.FilePage.ApplyButton);
        }

        [TearDown]
        public void TearDown()
        {
            _context?.Dispose();
        }
    }
}
