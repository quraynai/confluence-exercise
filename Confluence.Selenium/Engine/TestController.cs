﻿namespace Confluence.Selenium.Engine
{
    public abstract class TestController
    {
        protected Context GetContext()
        {
            var context = new Context();
            return context;
        }

        protected TestActor Run(Context context)
        {
            var testActor = new TestActor(context);
            return testActor;
        }
    }
}
