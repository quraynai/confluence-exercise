﻿using Confluence.Selenium.Component.Page;
using Confluence.Selenium.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Threading;

namespace Confluence.Selenium.Engine
{
    public class Context : IDisposable
    {
        private IWebDriver _webDriver;
        private WebDriverWait _waitDriver;

        public Context()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            _waitDriver = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
        }

        public Context Redirect(IPage page)
        {
            _webDriver.Navigate().GoToUrl(page.Url);
            return this;
        }

        public Context Click(By by, bool untilClickable = false)
        {
            var webElement = untilClickable ?
                FindElement(ExpectedConditions.ElementToBeClickable(by))
                : FindElement(by);
            webElement.Click();
            return this;
        }

        public Context Wait(int milliSeconds)
        {
            Thread.Sleep(milliSeconds);
            return this;
        }

        public Context Wait(By by, Until until)
        {
            switch (until)
            {
                case Until.Visible:
                    _waitDriver.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(by));
                    break;
                default:
                    throw new InvalidOperationException();
            }

            return this;
        }

        public Context Type(By by, string value, bool untilInteractable = false)
        {
            var webElement = untilInteractable ?
                FindElement(ExpectedConditions.ElementToBeClickable(by))
                : FindElement(by);
            webElement.SendKeys(value);
            return this;
        }

        public Context Type(string value)
        {
            new Actions(_webDriver).SendKeys(value).Perform();
            return this;
        }

        public Context ExpectText(By by, string expectedText, bool ignoreCase = false)
        {
            var element = FindElement(by);
            var actualText = element.Text;

            Assert.AreEqual(expectedText, actualText, ignoreCase);
            return this;
        }

        private IWebElement FindElement(Func<IWebDriver, IWebElement> func)
        {
            var webElement = func(_webDriver);
            if (webElement == null)
            {
                throw new ArgumentNullException($"Element cannot be found.");
            }

            return webElement;
        }

        private IWebElement FindElement(By by)
        {
            var webElement = _webDriver.FindElement(by);
            if (webElement == null)
            {
                throw new ArgumentNullException($"Element cannot be found.");
            }

            return webElement;
        }

        public void Dispose()
        {
            _webDriver?.Dispose();
        }
    }
}
