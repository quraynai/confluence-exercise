﻿using System;

namespace Confluence.Selenium.Engine
{
    public class TestActor
    {
        private readonly Context _context;

        public TestActor(Context context)
        {
            _context = context;
        }

        public TestActor Perform(string description, Action<Context> action)
        {
            action(_context);
            return this;
        }
    }
}
