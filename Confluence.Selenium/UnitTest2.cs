﻿using Confluence.Selenium.Component.Page;
using Confluence.Selenium.Engine;
using Confluence.Selenium.Model;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Confluence.Selenium
{
    [TestFixture]
    public class UnitTest2 : TestController
    {
        private Context _context;

        static object[] ExerciseSource =
        {
            new object[] { "quraynai@gmail.com", "test123*", "Try Selenium To Create New Page",
@"Quality Engineers at Atlassian are our (not so) secret weapon. They not only help teams continually
improve the quality of the software they create, but they also help teams do it more effectively. In short,
our Quality Engineers are essential for shipping quality software faster.
Our developers know this. QE is routinely voted the most satisfying aspect of how we develop software
at Atlassian. As such, developers constantly pull Quality engineers into collaborating on their work. This is
not a company where code is thrown over the wall to QA/QE, nor a place for people who don’t want to be
deeply involved in the development process.
To be effective, our Quality Engineers need strong testing, technical and collaboration skills. The aim of
this set of exercises is to allow you to demonstrate some of those. " },
        };

        [SetUp]
        public void SetUp()
        {
            _context = GetContext();
        }

        // [TestCaseSource("ExerciseSource")]
        public void New(string email, string password, string title, string body) => Run(_context)
            .Perform
            (
                description: "Go to login page",
                action: context => context.Redirect(Pages.LoginPage)
            )
            .Perform
            (
                description: "Type username",
                action: context => context.Type(Pages.LoginPage.UsernameTextbox, email)
            )
            .Perform(
                description: "Click continue",
                action: context => context.Click(Pages.LoginPage.SubmitButton)
            )
            .Perform(
                description: "Type password",
                action: context => context.Type(Pages.LoginPage.PasswordTextbox, password)
            )
            .Perform
            (
                description: "Click submit",
                action: context => context.Click(Pages.LoginPage.SubmitButton)
            )
            .Perform
            (
                description: "Wait for login",
                action: context => context.Wait(6000)
            )
            .Perform
            (
                description: "Redirect to main page",
                action: context => context.Redirect(Pages.MainPage)
            )
            .Perform
            (
                description: "Click create new item",
                action: context => context.Click(Pages.MainPage.CreateDialogButton)
            )
            .Perform
            (
                description: "Expect create dialog",
                action: context => context.Wait(Pages.MainPage.CreateDialog, Until.Visible)
            )
            .Perform
            (
                description: "Click new blank page",
                action: context => context.Click(Pages.MainPage.BlankPageTemplate)
            )
            .Perform
            (
                description: "Click create",
                action: context => context.Click(Pages.MainPage.CreateItemButton, untilClickable: true)
            )
            .Perform
            (
                description: "Wait for page to fully loaded",
                action: context => context.Wait(6000)
            )
            .Perform
            (
                description: "Type title",
                action: context => context.Type(Pages.BlankPage.Title, title, untilInteractable: true)
            )
            .Perform
            (
                description: "Type tab to body",
                action: context => context.Type(Keys.Tab)
            )
            .Perform
            (
                description: "Type body",
                action: context => context.Type(body)
            )
            .Perform
            (
                description: "Click publish",
                action: context => context.Click(Pages.BlankPage.PublishButton)
            )
            .Perform
            (
                description: "Wait for file page title",
                action: context => context.Wait(Pages.FilePage.Title, Until.Visible)
            )
            .Perform
            (
                description: "Expect title is matched",
                action: context => context.ExpectText(Pages.FilePage.Title, title, ignoreCase: true)
            );

        [TearDown]
        public void TearDown()
        {
            _context?.Dispose();
        }
    }
}
