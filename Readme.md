# Confluence Exercise

Project to support selenium test for confluence website. Only support window.


# Pre-requisites

 - Visual Studio 2017
 - .Net Framework 4.6.1
 - [Nuget Source](https://api.nuget.org/v3/index.json)

# Examples

 - [Context Runner](https://bitbucket.org/quraynai/confluence-exercise/src/master/Confluence.Selenium/UnitTest1.cs)
 - ~~[Actor Runner](https://bitbucket.org/quraynai/confluence-exercise/src/master/Confluence.Selenium/UnitTest2.cs)~~